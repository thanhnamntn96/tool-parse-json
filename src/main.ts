import './style.css';
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueLazyLoad from 'vue3-lazyload'
import PrimeVue from 'primevue/config';
import Tailwind from 'primevue/passthrough/tailwind';
import Textarea from 'primevue/textarea';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';

const app = createApp(App)
app.use(router)
app.use(VueLazyLoad, {});
app.component('Textarea', Textarea);
app.component('Button', Button);
app.component('InputText', InputText);
app.use(PrimeVue, { unstyled: true, pt: Tailwind });

app.mount('#app')
